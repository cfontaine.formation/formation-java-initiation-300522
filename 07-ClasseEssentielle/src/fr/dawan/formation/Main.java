package fr.dawan.formation;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {	
        // Wrapper
        Integer iWarp = new Integer(12);
        System.out.println(iWarp);

        // Convertion String en type primitif
        double dC = Double.parseDouble("12.3");
        System.out.println(dC);

        // Convertion String vers Wrapper
        Double dcWarp = Double.valueOf("45.6");
        System.out.println(dcWarp);

        // Conversion Double vers long
        long ll = dcWarp.longValue();
        System.out.println(ll);
        
        // Affichage d'un nombre au format binaire, hexadecimal, octal
        String strBin = Integer.toBinaryString(12);
        String strHex = Integer.toHexString(12);
        String strOct = Integer.toOctalString(12);
        System.out.println(strBin + " " + strHex + " " + strOct);

		
		// Chaine de caractères
		String str1 = "Hello World";
		String str2 = new String("Bonjour");

		// Les chaines de caractères sont immuables une fois créée elles ne peuvent plus
		// être modifiées
		str1.toLowerCase();
		str1.substring(4);
		System.out.println(str1);

		// length -> Nombre de caractère de la chaine de caractère
		System.out.println(str1.length()); // 11

		// Concaténation
		System.out.println(str1 + str2); // + concaténation
		System.out.println(str1.concat(str2));

        // La méthode join concatène les chaines, en les séparants par une chaine de
        // séparation
		String str3 = String.join(";", "azerty", "dfghj", "xcvbn");
		System.out.println(str3);	// azerty;dfghj;xcvbn

		 // Découpe la chaine et retourne un tableau de sous-chaine suivant un séparateur
		String[] tabStr = str3.split(";");
		for (String s : tabStr) {
			System.out.println(s);
		}

		// substring permet d'extraire une sous-chaine
        // de l'indice passé en paramètre jusqu'à la fin de la chaine
		System.out.println(str1.substring(6)); // World
		// de l'indice passé en paramètre jusqu'à l'indice de fin (exclut)
		System.out.println(str1.substring(6, 9)); // Wo

        // startsWith retourne true, si la chaine commence par la chaine passé en
        // paramètre
		System.out.println(str1.startsWith("Hello")); // true
		System.out.println(str1.startsWith("aaaaa")); // false

		// indexOf retourne la première occurence de la chaine ou (de caractère) passée en paramètre
		System.out.println(str1.indexOf('o')); // 4
		System.out.println(str1.indexOf("o", 5)); // idem mais à partir de l'indice passé en paramètre 7
		System.out.println(str1.indexOf("o", 8)); // retourne -1, si le caractère n'est pas trouvé

		// Remplace toutes les les sous-chaines target par replacement
		System.out.println(str1.replace('o', 'a'));		// Hella Warld
		System.out.println(str1.replace("Wo", "__"));	// Hello __rld

		// Retourne le caractère à l'indice 1
		System.out.println(str1.charAt(1)); // e

		// Retourne true, si la sous-chaine passée en paramètre est contenu dans la
        // chaine
		System.out.println(str1.contains("Wo")); // true
		System.out.println(str1.contains("aa")); // false

		// Retourne la chaine en majuscule
		System.out.println(str1.toLowerCase()); // hello world
		System.out.println(str1.toUpperCase()); // HELLO WORLD

		// trim supprime les caractères de blanc du début et de la fin de la chaine
		String str4 = "   \t   \n Hello world    \t \n \t \n";
		System.out.println(str4);
		System.out.println(str4.trim());

		// Retourne une chaine formater
        // %d -> entier, %s -> String, %f -> réel, %c -> caratère ...
		int val = 42;
		System.out.println(String.format("%d [%s] => %f", val, "Bonjour", 12.3));

		str4 = null;
		System.out.println("Hello World".equals(str4)); // false
		// System.out.println(str4.equals("Hello World"));
		System.out.println("Hello World".equals(str1)); // true

		System.out.println(str1.compareTo(str2)); // >0
		System.out.println(str2.compareTo(str1)); // <0
		System.out.println(str2.compareTo("Bonjour")); // ==0

		// On peut chainer les méthodes
		String str = "abcd".concat("ef   ").toUpperCase().trim();
		System.out.println(str);

		// StringBuilder
        // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation
        // et insertion, remplacement,supression de sous-chaine)
        // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un
        // objet String => pas de création d'objet intermédiare
		StringBuilder sb = new StringBuilder("hello");
		sb.append(' ');
		sb.append("World");
		sb.append(false);
		sb.append(42);
		sb.delete(5, 6);
		sb.insert(5, "_______");
		System.out.println(sb);
		String str6 = sb.toString();
		System.out.println(str6.toUpperCase());

		// StringTokenizer
        // Permet de décomposer une chaîne de caractères en une suite de mots séparés
        // par des délimiteurs
		StringTokenizer st = new StringTokenizer("azerty;dfghj;xcvbn;gerfhvcjv;hdhjsbcd;vdcvjv", ";");
		System.out.println(st.countTokens());
		while (st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}

		// Excercice Chaine de caractère
		System.out.println(inverser("Bonjour"));
		System.out.println(Palindrome("Radar"));
		System.out.println(Palindrome("SOS"));
		System.out.println(Palindrome("Bonjour"));

		System.out.println(isPalindrome("radar"));
		System.out.println(isPalindrome("SOS"));
		System.out.println(isPalindrome("Bonjour"));

		// Date => JDK 1.0
		Date d = new Date();
		System.out.println(d);

		// à partir de java 8 => LocalDate, LocalTime, LocalDateTime
		LocalDate dateDuJour = LocalDate.now();	// now => obtenir la date courante
		System.out.println(dateDuJour);

		// Nombre de jour depuis 1 janvier 1970
		LocalDate date2 = LocalDate.ofEpochDay(3000L);
		System.out.println(date2);

		// of => créer une date spécifique
		LocalDate dernierJour2022 = LocalDate.of(2022, Month.DECEMBER, 31);
		System.out.println(dernierJour2022);

		LocalTime currentTime = LocalTime.now();	 // heure courante
		System.out.println(currentTime);

		// On manipule une date avec les méthodes plusXXXXX et minusXXXXX
		LocalDate d1 = dateDuJour.plusYears(1).plusMonths(3).minusDays(5);
		System.out.println(d1);

		// Period => représente une durée
		Period troisSemaine = Period.ofWeeks(3);
		LocalDate d2 = dateDuJour.plus(troisSemaine);  // plus et minus permettent d'ajouter ou retirer une Period
		System.out.println(d2);
		
		// between -> obtenir une durée entre 2 days
		Period p2 = Period.between(dateDuJour, dernierJour2022);
		System.out.println(p2);

		// LocalDate -> String
		System.out.println(dateDuJour.format(DateTimeFormatter.ISO_LOCAL_DATE));
		System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
		System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
		System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("dd/MM/YY")));
		System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("dd MMM YYYY")));

		// String -> LocalDate
		LocalDate d3 = LocalDate.parse("2022-09-01", DateTimeFormatter.ISO_LOCAL_DATE);
		System.out.println(d3);

		// Heure en fonction des fuseaux horraires
		LocalTime t1 = LocalTime.now(ZoneId.of("GMT+4"));
		System.out.println(t1);

		// Math
		double rand = Math.random();
		System.out.println(rand);
		int valAl = (int) (Math.random() * 100.0);
		System.out.println(valAl);
		int valAl2 = (int) (20.0+Math.random() * 80.0);
		System.out.println(valAl2);
	}

//  Exercice Inversion de chaine
//  Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés  
//  exemple : bonjour => ruojnob
	public static String inverser(String str) {
		StringBuilder sb = new StringBuilder();
		for (int i = str.length() - 1; i >= 0; i--) {
			sb.append(str.charAt(i));
		}
		return sb.toString();
	}

//  Exercice Palindrome
//  Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
// exemple : SOS, radar

	public static boolean Palindrome(String str) {
		String tmp = str.trim().toLowerCase();
		return tmp.equals(inverser(tmp));
	}

	// version + rapide
	public static boolean isPalindrome(String str) {

		for (int i = 0; i < (str.length() / 2); i++) {
			if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
				return false;
			}
		}
		return true;
	}

}
