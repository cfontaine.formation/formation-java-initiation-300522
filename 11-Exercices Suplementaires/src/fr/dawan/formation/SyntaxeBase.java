package fr.dawan.formation;

import java.util.Scanner;

public class SyntaxeBase {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		// Permutation de 2 variables
		// Saisir deux variables de type entier et les permuter avant de les afficher
		System.out.print("a=");
		int a = scan.nextInt();
		System.out.print("b=");
		int b = scan.nextInt();
		int tmpo = a;
		a = b;
		b = tmpo;
		System.out.println("a= " + a + " b= " + b);

		// Permutation de 2 octets
		// - Permutez les deux octets d'une variable entière de 2 octets saisis par l'utilisateur
		// - Afficher la valeur avant et après la permutation en hexadécimal et en binaire
		System.out.print("valeur= ");
		short valeur = scan.nextShort();
		System.out.println(Integer.toBinaryString(valeur) + " " + Integer.toHexString(valeur));
		int permOct1 = (valeur << 8) & 0xFF00;
		int permOct2 = (valeur >>> 8) & 0xFF;
		int res = (permOct1 | permOct2);
		System.out.println(Integer.toBinaryString(res) + " " + Integer.toHexString(res));

		// Trie de 3 Valeurs
		// Saisir 3 nombres et afficher ces nombres triés dans l'ordre croissant sous la
		// forme 1.5 < 10.5 < 50.7
		System.out.print("v1= ");
		double v1 = scan.nextDouble();
		System.out.print("v2= ");
		double v2 = scan.nextDouble();
		System.out.print("v3= ");
		double v3 = scan.nextDouble();
		if (v1 > v2) {
			double tmp = v1;
			v1 = v2;
			v2 = tmp;
		}
		if (v3 < v1) {
			System.out.println(v3 + " < " + v1 + " < " + v2);
		} else if (v3 > v2) {
			System.out.println(v1 + " < " + v2 + " < " + v3);
		} else {
			System.out.println(v1 + " < " + v3 + " < " + v2);
		}

		// Écrire un programme java qui
		// - saisie trois notes à partir du clavier
		// - calcule et affiche la moyenne de ces notes
		// - affiche la mention :
		// - Bien Si la moyenne est >12
		// - Passable Si la moyenne est comprise entre 10 et 12
		// - Non admis Si la moyenne est <10
		System.out.print("note 1= ");
		double val1 = scan.nextDouble();
		System.out.print("note 2= ");
		double val2 = scan.nextDouble();
		System.out.print("note 3= ");
		double val3 = scan.nextDouble();

		double moyenne = (val1 + val2 + val3) / 3.0;
		System.out.println(moyenne);

		if (moyenne < 10.0) {
			System.out.println("Non admis");
		} else if (moyenne >= 10.0 && moyenne <= 12.0) {
			System.out.println("Passable");
		} else {
			System.out.println("Bien");
		}

		// Série harmonique
		// On saisit un nombre entier n et on calcule la somme des n premiers termes de
		// la série harmonique:
		// 1 + 1/2 + 1/3 + 1/4 + ... + 1/n
		System.out.print("n=");
		int n = scan.nextInt();
		double harmonique = 1.0;
		for (int i = 1; i < n; i++) {
			harmonique += 1.0 / n;
		}
		System.out.println("harmonique=" + harmonique);

		// Afficher les nombres d'un intervalle
		// Saisir 2 nombres entier et afficher tous les nombres entiers entre le nombre
		// saisi le plus grand et le plus petit
		// 		23 26 donne 26 25 24 23
		// 		34 31 donne 34 33 32 31
		System.out.print("n1=");
		int n1 = scan.nextInt();
		System.out.print("n2=");
		int n2 = scan.nextInt();
		if (n1 > n2) {
			int tmp = n1;
			n1 = n2;
			n2 = tmp;
		}
		for (int i = n2; i >= n1; i--) {
			System.out.print(i + " ");
		}

		// Écrire une fonction EstPresent elle prend en paramètre un tableau de caractère et un caractère
		// Elle retourne true, si le caractère passé en paramètre est présent dans le tableau
		char t[] = { 'a', 'z', 'e', 'r', 't', 'y' };
		System.out.println(estPresent(t, 'a')); // true
		System.out.println(estPresent(t, 'e')); // true
		System.out.println(estPresent(t, 'y')); // true
		System.out.println(estPresent(t, 'u')); // false

		scan.close();

	}

	public static boolean estPresent(char[] t, char c) {
		for (int i = 0; i < t.length; i++) {
			if (t[i] == c) {
				return true;
			}
		}
		return false;
	}

}
