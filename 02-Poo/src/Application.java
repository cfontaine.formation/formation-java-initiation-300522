
public class Application {

	public static void main(String[] args) {
		
		// Appel d'une méthode de classe
		Voiture.testMethodeClasse();
		
		System.out.println("Compteur Voiture"+ Voiture.getCompteurVoiture()); // Voiture.compteurVoiture
		
		// Instantiation de la classe Voiture
		Voiture v1=new Voiture();
		System.out.println("Compteur Voiture"+  Voiture.getCompteurVoiture());
		v1.afficher();
		
		// Encapsulation => Pour modifier les attributs, on doit passer par les setters
		v1.setVitesse(10);//v1.vitesse=10;
		// v1.marque="Honda";	 // n'est plus accessible => privé
		System.out.println(v1.getVitesse()); // v1.vitesse
		System.out.println(v1.getMarque()); // v1.marque
		
		// Appel d'une méthode d'instance
		v1.accelerer(20);
		v1.afficher();
		v1.freiner(15);
		System.out.println(v1.estArreter());
		v1.arreter();
		System.out.println(v1.estArreter());
		v1.afficher();
		
		Voiture v2=new Voiture();
		System.out.println("Compteur Voiture"+  Voiture.getCompteurVoiture());
		v2.setVitesse(10);	//v2.vitesse=20;
		System.out.println(v2.getVitesse()); //v2.vitesse
		
		Voiture v3=new Voiture("fiat", "Jaune", "fr-1024-ab");
		System.out.println("Compteur Voiture"+  Voiture.getCompteurVoiture()); //Voiture.compteurVoiture
		v3.afficher();
		
		Voiture v4=new Voiture("toyota","noir","fr-0684-FR",10,300);
		System.out.println("Compteur Voiture"+  Voiture.getCompteurVoiture());
		v4.afficher();
		
		// Appel explicit au Garbage collector => à eviter
		// System.gc();
		
		// Méthode de classe
		System.out.println(Voiture.egalVitesse(v1, v2));
		
		// Exercice: Compte Bancaire
		CompteBancaire cb=new CompteBancaire(150.0,"John Doe");
		cb.afficher();
		//cb.solde=150.0;
		//cb.iban="fr-6259800000";
		//cb.titulaire="John Doe";
		
		cb.crediter(150.0);
		cb.afficher();
		cb.debiter(200.0);
		cb.afficher();
		System.out.println(cb.estPositif());
		
		CompteBancaire cb2=new CompteBancaire("Jane Doe");
		cb2.afficher();
	}
}
