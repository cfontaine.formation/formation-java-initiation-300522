import java.io.Serializable;

public class CompteBancaire implements Serializable{

	private static final long serialVersionUID = 1L;

	private double solde = 50.0;
	private String iban;
	private String titulaire;
	
	private static int cpt;
	
	public CompteBancaire() {
		cpt++;
		iban="fr-5900-"+ cpt;
	}

	public CompteBancaire(String titulaire) {
		this();
		this.titulaire = titulaire;
	}

	public CompteBancaire(double solde, String titulaire) {
		this(titulaire);
		this.solde = solde;
	}

	
	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}

	public void afficher() {
		System.out.println("_____________________");
		System.out.println("solde=" + solde);
		System.out.println("iban=" + iban);
		System.out.println("tituaire=" + titulaire);
		System.out.println("_____________________");
	}

	public void crediter(double somme) {
		if (somme > 0.0) {
			solde += somme;
		}
	}

	public void debiter(double somme) {
		if (somme > 0.0) {
			solde -= somme;
		}
	}

	public boolean estPositif() {
		return solde >= 0.0;
	}
}
