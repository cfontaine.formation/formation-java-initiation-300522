import java.io.Serializable;

public class Voiture implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// Variable d'instance (attribut)

	// L'encapsulation consiste à cacher l'état interne d'un objet et d'imposer de
	// passer par des méthodes permettant un accès sécurisé à l'état de l'objet
	// private => attribut accessible seulement dans la classe elle-même
	private String marque;
	private String couleur = "Rouge"; // Rouge valeur par défaut de l'attribut couleur
	private String plaqueIma; // par défaut String à pour valeur null (référence
	private int vitesse; // par défaut int a pour valeur 0
	private int compteurKM = 20;

	// Variable de classe
	private static int compteurVoiture;

	// Constructeurs
	// Avec eclipse, on peut générer les constructeurs
	// Menu contextuel -> Source -> Generarted constructror using fields...

	// Constructeur par défaut (sans paramètres)
	public Voiture() {
		compteurVoiture++;
		System.out.println("Constructeur par défaut");
	}

	// On peut surcharger le constructeur
	Voiture(String marque, String couleur, String plaqueIma) {
		this();// Appel d'un autre constructeur avec this, doit être la première instruction du
				// constructeur ici on appel le constructeur par défaut

		System.out.println("Constructeur 3 paramètres");
		this.marque = marque;	// utilisation de this pour indiquer que l'on acccède à la variable d'instance
		this.couleur = couleur;
		this.plaqueIma = plaqueIma;
	}

	public Voiture(String marque, String couleur, String plaqueIma, int vitesse, int compteurKM) {
		this(marque, couleur, plaqueIma);
//      this.marque = marque;
//      this.couleur = couleur;
//      this.plaqueIma = plaqueIma;
		System.out.println("Constructeur 5 paramètres");
		this.vitesse = vitesse;
		this.compteurKM = compteurKM;
	}

    // Getter / Setter
    // Un getter permet l'accès en lecture à un attribut
	public String getMarque() {
		return marque;
	}
	
	// Un setter permet l'accès en écriture à un attribut
	public void setMarque(String marque) {
		this.marque = marque;
	}

	public int getVitesse() {
		return vitesse;
	}

	public void setVitesse(int vitesse) {
		if (vitesse > 0) {
			this.vitesse = vitesse;
		}
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getPlaqueIma() {
		return plaqueIma;
	}

	public void setPlaqueIma(String plaqueIma) {
		this.plaqueIma = plaqueIma;
	}

	public int getCompteurKM() {
		return compteurKM;
	}
	
	public void setCompteurKM(int compteurKM) {
		this.compteurKM = compteurKM;
	}

	public static int getCompteurVoiture() {
		return compteurVoiture;
	}

	public static void setCompteurVoiture(int compteurVoiture) {
		Voiture.compteurVoiture = compteurVoiture;
	}

	// Méthodes d'instance
	public void accelerer(int vAcc) {
		if (vAcc > 0) {
			vitesse += vAcc; // vitesse=vitesse+vAcc
		}
	}

	public void freiner(int vFrn) {
		if (vFrn > 0) {
			vitesse -= vFrn;
		}
	}

	public void arreter() {
		vitesse = 0;
	}

	public boolean estArreter() {
		return vitesse == 0;
	}

	public void afficher() {
		System.out.println(
				"Voiture[" + marque + " " + couleur + " " + plaqueIma + " " + vitesse + " " + compteurKM + "]");
	}

	// Méthode de classe
	
	public static void testMethodeClasse() {
		System.out.println("Méthode de classe");
		System.out.println(compteurVoiture); // variable de classe
		// inaccéssible directement dans une méthode de classe
        // Les méthodes et les variables ne peuvent pas être utilisées dans une méthode  de classe
        // System.out.println(vitesse); // Erreur=> une méthode de classe ne peut pas accèder à une variable d'instance
        // accelerer(10);                   // ou à une méthode d'instance

	}
	
	public static boolean egalVitesse(Voiture a, Voiture b) {
		// Dans un méthode de classe on peut accèder à une variable d'instance
        // si la référence d'un objet est passée en paramètre
		return a.vitesse == b.vitesse;
	}

//	public static void main(String [] args) {
//		Voiture v1=new Voiture();
//		v1.afficher();
//	}
}
