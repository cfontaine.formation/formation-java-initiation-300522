import java.util.Scanner;

public class Methode {

	public static void main(String[] args) {
		// Appel d'une méthode
		double m = multiplier(2.0, 3.0);
		System.out.println(m);
		System.out.println(multiplier(4.5, 6.7));

		// Appel d'une méthode sans type retour (void)
		int v = 12;
		afficher(v);

		// Test des arguments par valeurs
		testParamValue(v); // La valeur contenu dans vi est copier dans le paramètre (a)
		System.out.println(v); // le contenu de vi ne peut pas être modifié par la méthode => passage par valeur

        // test des arguments par valeurs (paramètre de type référence)
        StringBuilder b = new StringBuilder("Value");
        testParamValueRef(b);
        System.out.println(b);

        // test des arguments par valeurs (Modification de l'état de l'objet)
        testParamValueRefObj(b);
        System.out.println(b);

		
		// Exercice: appel de la méthode maximum
		Scanner scn = new Scanner(System.in);
		int v1 = scn.nextInt();
		int v2 = scn.nextInt();
		System.out.println(maximum(v1, v2));

		// Exercice: appel de la méthode paire
		System.out.println(even(3));
		System.out.println(even(4));

		// Nombre d'arguments variable
		System.out.println(moyenne(1));
		System.out.println(moyenne(1, 2));
		System.out.println(moyenne(4, 2, 5, 6));
		int t[] = { 12, 4, 6, 7 };
		System.out.println(moyenne(4, t));

		// Surcharge de méthode
		System.out.println(somme(1, 2));
		System.out.println(somme(1.5, 2.7));
		System.out.println(somme(1, 2.7));
		System.out.println(somme("hell", "o"));
		System.out.println(somme(1, 2, 3));

		// Si le compilateur ne trouve pas de corresponcance exacte avec le type des paramètres
		// il effectue des conversions automatique pour trouver la méthode à appeler
		System.out.println(somme(1, 3L)); // appel de la méthode => somme(double, double )
		System.out.println(somme(1L, 2L)); // appel de la méthode => somme(double, double )
		System.out.println(somme('a', 'b')); // appel de la méthode => somme(double, double )

	     // Si le compilateur ne trouve pas de méthode correspondante aux paramètres => ne compile pas
		 // System.out.println(somme("Hello", 3));

		// Affichage des paramètres passés à la méthode main
		for (String a : args) {
			System.out.println(a);
		}

		// Méthode Récursive
		int res = factorial(3);
		System.out.println(res);

		// Exercice: tableau
		menu(scn);

		scn.close();
	}

	// Déclaration d'une méthode
	public static double multiplier(double v1, double v2) {
		return v1 * v2; // return => Interrompt l'exécution de la méthode
						// => Retourne la valeur (expression à droite)
	}

	public static void afficher(int a) {
		System.out.println(a);
		// return; // avec void => return; ou return peut être omis
	}

	// test des arguments par valeurs (paramètre de type primitif)
	public static void testParamValue(int p) {
		System.out.println(p);
		p = 23;	 // la modification du paramètre p n'a pas de répercution en dehors de la méthode
		System.out.println(p);
	}
	
	 // test des arguments par valeurs (paramètre de type référence)
	public static void testParamValueRef(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie la valeur de la référence sb, il n'a pas répercution en dehors de la méthode
        // la référence est modifiée
        sb = new StringBuilder("Other Value");
        System.out.println(sb);
    }

    // test des arguments par valeurs (Modification de l'état de l'objet)
	public static void testParamValueRefObj(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie l'état de l'objet, il a une répercution en dehors de la méthode (la référence n'est pas modifiée)
        sb.append("et Other Value");
        System.out.println(sb);
    }
	
	// Exercice Maximum
	// Écrire une fonction maximum qui prends en paramètre 2 nombres et retourne le maximum
	// Saisir 2 nombres et afficher le maximum entre ces 2 nombres
	public static int maximum(int a, int b) {
//		if (a < b) {
//			return b;
//		} else {
//			return a;
//		}
		return a < b ? b : a;
	}

	// Exercice Paire
	// Écrire une fonction Even qui prend un entier en paramètre
	// elle retourne vrai si il est paire
	public static boolean even(int a) {
//		if(a%2==0) {
//			return true;
//		}
//		else {
//			return false;
//		}
		// return (a&1)==0;
		return a % 2 == 0;
	}

	// Nombre d'arguments variable
	public static double moyenne(int v, int... vp) {
		// dans la méthode vp est considérée comme un tableau
		double somme = v;
		for (int elm : vp) {
			somme += elm;
		}
		return somme / (vp.length + 1);
	}

	// Surcharge de méthode
	// Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même
	// nom, mais leurs signatures doient être différentes
	// La signature d'une méthode correspond aux types et nombre de paramètres
	// Le type de retour ne fait pas partie de la signature
	public static int somme(int a, int b) {
		System.out.println("2 entiers");
		return a + b;
	}

	public static double somme(double a, double b) {
		System.out.println("2 doubles");
		return a + b;
	}

	public static double somme(int a, double b) {
		System.out.println("1 entier 1 double");
		return a + b;
	}

	public static String somme(String str1, String str2) {
		System.out.println("2 chaines");
		return str1 + str2;
	}

	public static int somme(int v, int w, int x) {
		System.out.println("3 entiers");
		return v + w + x;
	}

	// Méthode récursive
	public static int factorial(int n) { // factoriel= 1* 2* … n
		if (n <= 1) { // condition de sortie
			return 1;
		} else {
			return factorial(n - 1) * n;
		}
	}

//  Tableau
//  - Ecrire un méthode qui affiche un tableau d’entier passé en paramètre
//  - Ecrire une méthode qui permet de saisir :
//        - La taille du tableau
//        - Les éléments du tableau
//  - Ecrire une méthode qui calcule le maximum et la moyenne 
//  - Faire un menu qui permet de lancer ces méthodes:Faire un menu qui permet de lancer ces méthodes
	public static void afficherTab(int[] t) {
		System.out.print("[ ");
		for (int e : t) {
			System.out.print(e + " ");
		}
		System.out.println("]");
	}

	public static int[] saisieTableau(Scanner sc) {
		int size = sc.nextInt();
		int tmp[] = new int[size];
		for (int i = 0; i < size; i++) {
			System.out.print("T[" + i + "]=");
			tmp[i] = sc.nextInt();
		}
		return tmp;
	}

	public static int maximum(int t[]) {
		int max = Integer.MIN_VALUE;
		for (int e : t) {
			if (e > max) {
				max = e;
			}
		}
		return max;
	}

	public static double moyenne(int t[]) {
		double somme = 0;
		for (int e : t) {
			somme += e;
		}
		return somme / t.length;
	}

	public static void affMenu() {
		System.out.println("1 Saisie du tableau");
		System.out.println("2 Afficher le tableau");
		System.out.println("3 Calculer le maximum et la moyenne");
		System.out.println("4 Quitter");
	}

	public static void menu(Scanner sc) {
		affMenu();
		int choix;
		int tab[] = null;
		do {
			choix = sc.nextInt();
			switch (choix) {
			case 1:
				tab = saisieTableau(sc);
				break;
			case 2:
				if (tab != null) {
					afficherTab(tab);
				}
				break;
			case 3:
				if (tab != null) {
					System.out.println("Maximum=" + maximum(tab) + "Moyenne=" + moyenne(tab));
				}
				break;
			case 4:
				System.out.println("Au revoir!");
				break;
			default:
				affMenu();
				System.out.println("Choix incorrect");
			}
		} while (choix != 4);
		sc.close();
	}
}
