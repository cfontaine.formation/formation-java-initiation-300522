
public class TypeBase {

	// Méthode main => point d'entré du programme
	public static void main(String[] args) {

		// Déclaration de variable
		int i;

		// System.out.println(i); // En java, on ne peut pas utiliser une variable non
		// initlisée

		// Initialisation de la variable
		i = 42;
		System.out.println(i);

		// Déclaration et initialisation d'une variable
		double d = 3.14;
		System.out.println(d);

		// Déclaration multiple
		double largeur = 12.3, hauteur = 34.9;
		System.out.println("largeur=" + largeur + "hauteur=" + hauteur);

		// Littérale
		// Littérale entière est par défaut de type int
		long l = 123456789012L; // L-> litéral de type long
		System.out.println(l);

		// Littéraux entier changement
		int dec = 12; // décimal base 10
		int hex = 0xf4; // 0x -> héxadécimal base 16
		int oct = 0123; // 0 -> octal base 8
		int bin = 0b101011; // 0b -> binaire base 2
		System.out.println(dec + " " + hex + " " + oct + " " + bin);

		// Littéral réel est par défaut de type double
		float f = 1.23F; // F -> litéral de type float
		System.out.println(f);

		// Littéral réel
		double dd1 = 12.34;
		double dd2 = .56;
		double dd3 = 1.23e3; // 1230
		System.out.println(dd1 + " " + dd2 + " " + dd3);

		// Littéral caractère
		char chr = 'A';
		char chrUtf8 = '\u0061';
		System.out.println(chr + " " + chrUtf8);

		// Littéral Boolean
		boolean tst = true; // false
		System.out.println(tst);

		// Transtypage implicite => pas de perte de donnée
		// Type inférieur vers un type supérieur
		int ti1 = 34;
		long ti2 = ti1;
		
		// Entier vers un réel
		double ti3 = ti1;
		System.out.println(ti1 + " " + ti2 + " " + ti3);

		// Transtype explicite cast => (nouveauType)
		// Réel vers entier
		double te1 = 12.3;
		int te2 = (int) te1;
		System.out.println(te1 + " " + te2);

		// Type supérieur vers un type inférieur
		byte te3 = (byte) te2;
		System.out.println(te2 + " " + te3);

		// Transtype explicite: Dépassement de capacité
		int dep1 = 300;				// 00000000 00000000 00000001 00101100 -> 300
		byte dep2 = (byte) dep1;	//                            00101100 -> 44
		System.out.println(dep1 + " " + dep2);

		// Type référence
		StringBuilder sb1 = new StringBuilder("hello");
		StringBuilder sb2 = null;	 // sb2 ne référence aucun objet
		System.out.println(sb1 + " " + sb2);
		sb2 = sb1;	// sb1 et sb2 font références au même objet
		System.out.println(sb1 + " " + sb2);
		sb1 = null;
		System.out.println(sb1 + " " + sb2);
		sb2 = null;
		// sb1 et sb2 sont égales à null
        // Il n'y a plus de référence sur l'objet => Il éligible à la destruction par le garbage collector
	}

}
