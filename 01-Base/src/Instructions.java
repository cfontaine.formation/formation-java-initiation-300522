import java.util.Scanner;

public class Instructions {

	public static void main(String[] args) {
		// Condition: if
		double d = 10.0;
		if (d > 10.0) {
			System.out.println("sup à 10");
		} else if (d < 10.0) {
			System.out.println("inf à 10");
		} else {
			System.out.println("égal  à 10");
		}

		// Exercice: Trie de 2 valeurs
		// Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant
		// sous la forme 1.5 < 10.5
		Scanner sc = new Scanner(System.in);
		double d1 = sc.nextDouble();
		double d2 = sc.nextDouble();
		if (d1 < d2) {
			System.out.println(d1 + " < " + d2);
		} else if (d1 > d2) {
			System.out.println(d2 + " < " + d1);
		} else {
			System.out.println(d2 + " = " + d1);
		}

		// Exercice: Intervalle
		// Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus)
		int v = sc.nextInt();
		if (v > -4 && v <= 7) {
			System.out.println(v + " fait parti de l'interval -4 , 7");
		}

		// Condition switch
		int jours = 1;
		switch (jours) {
		case 1:
			System.out.println("Lundi");
			// break; // Comme il n'y a pas de break le code continura à s'executer jusqu'a
			// la fin du switch ou jusqu'au prochain break
		case 6:
		case 7:
			System.out.println("week end !");
			break;
		default:
			System.out.println("autre jour");
			break;
		}

		// Exercice Calculatrice
		// Faire un programme calculatrice
		// Saisir dans la console
		// - un double v1
		// - une chaine de caractère opérateur qui a pour valeur valide : + - * /
		// - un double v2
		// Afficher:
		// - Le résultat de l’opération
		// - Une message d’erreur si l’opérateur est incorrecte
		// - Une message d’erreur si l’on fait une division par 0

		double v1 = sc.nextDouble();
		String op = sc.next();
		double v2 = sc.nextDouble();
		switch (op) {
		case "+":
			System.out.println(v1 + " + " + v2 + "= " + (v1 + v2));
			break;
		case "-":
			System.out.println(v1 + " - " + v2 + "= " + (v1 - v2));
			break;
		case "*":
			System.out.println(v1 + " * " + v2 + "= " + (v1 * v2));
			break;
		case "/":
			if (v2 == 0.0) { // (v2>-0.000000001) && (v2<0.000000001)
				System.out.println("Division par 0");
			} else {
				System.out.println(v1 + " / " + v2 + "= " + (v1 / v2));
			}
			break;
		default:
			System.out.println(op + " n'est pas un opérateur valide");
		}

		// Opérateur ternaire
		double val = sc.nextDouble();
		double vabs = val > 0.0 ? val : -val;
		System.out.println(vabs);

		// Boucle while
		int in = 0;
		while (in < 10) {
			System.out.println(in);
			in++;
		}
		System.out.println("in=" + in);

		// Boucle do while
		in = 0;
		do {
			System.out.println(in);
			in++;
		} while (in < 10);
		System.out.println("in=" + in);

		// Boucle for
		for (int i = 0; i < 5; i++) {
			System.out.println("i=" + i);
		}

		// Instructions de branchement
		// break
		for (int i = 0; i < 10; i++) {
			System.out.println("i=" + i);
			if (i == 3) {
				break; // break => termine la boucle
			}
		}

		// continue
		for (int i = 0; i < 10; i++) {

			if (i == 3) {
				continue; // continue => on passe à l'itération suivante
			}
			System.out.println("i=" + i);
		}

		// Label
		// break avec Label:
		EXIT_LOOP: for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 10; i++) {
				System.out.println("i=" + i);
				if (i == 3) {
					break EXIT_LOOP; // se branche sur le label EXIT_LOOP et quitte les 2 boucles imbriquées
				}
			}
		}

//		Table de multiplication
//		Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9  
//
//		    1 X 4 = 4
//		    2 X 4 = 8
//		    …
//		    9 x 4 = 36
//			
//		Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
		while (true) {
			int mul = sc.nextInt();
			if (mul < 1 || mul > 9) {
				break;
			}
			for (int m = 1; m < 10; m++) {
				int res = m * mul;
				System.out.println(mul + " x " + m + " = " + res);
			}
		}

//		Exercice Quadrillage 
//		Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne  
//
//		    ex: pour 2 3  
//		    [ ][ ]  
//		    [ ][ ]  
//		    [ ][ ] 
		int col = sc.nextInt();
		int row = sc.nextInt();
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				System.out.print("[ ]");
			}
			System.out.println();
		}

		sc.close();
	}

}
