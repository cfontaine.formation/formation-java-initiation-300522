import java.util.Scanner;

public class Operateurs {

	public static void main(String[] args) {
		
		// Opérateur arithmétique
		int a = 1;
		int b = 2;
		int c = a + b;
		System.out.println(c);

		// Division par 0: entier -> exception
		// System.out.println(10/0);

		// Division par 0: Nombre à virgule flottante
		System.out.println(0.0 / 0.0); // NAN
		System.out.println(1.0 / 0.0); // INFINITY
		System.out.println(-1.0 / 0.0); // -INFINITY

		// Incrémenration/ Décrémentation
		// Pré-incrémention
		int inc = 0;
		int res = ++inc; 	// Incrémentation de inc et affectation de res avec la valeur de inc
		System.out.println("inc=" + inc + "res=" + res);	// inc= 1 res=1
		// Post-incrémentation
		inc = 0;
		res = inc++;		// Affectation de res avec la valeur de inc et incréméntation de inc 
		System.out.println("inc=" + inc + "res=" + res);	// res=0 inc=1

		// Affectation composé
		int af = 12;
		res += af; // res=res+af;

		// Opérateur comparaison
		// Une comparaison a pour résultat un booléen
		boolean tst1 = res > 5;
		System.out.println(tst1);

        // Opérateur logique
        // ! => Opérateur non
		System.out.println(!tst1);

        // c1       c2    |  ET    |  OU      | Ou exclusif
        //-------------------------------------------------
        // faux     faux  |  faux  |  faux    | faux
        // faux     vrai  |  faux  |  vrai    | vrai
        // vrai     faux  |  faux  |  vrai    | vrai
        // vrai     vrai  |  vrai  |  vrai    | faux

		
        // Opérateur court-circuit => && et ||
		double sd = 123.4;
		// && -> Opérateur et
		// && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
		boolean tst2 = res > 100 && sd < 1000.5;
		System.out.println("tst2=" + tst2);	// false
		
		// || -> Opérateur ou
	    // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
		boolean tst3 = res < 100 || sd < 0.0; // true
		System.out.println("tst3=" + tst3);	// false
		
		// Opérateur binaire
		int bin = 0b10101;
		System.out.println(Integer.toBinaryString(~bin));			// complémént -> 11111111 11111111 11111111 11101010
		System.out.println(Integer.toBinaryString(bin & 0b100));	// et -> 100
		System.out.println(Integer.toBinaryString(bin | 0b100));	// ou -> 10101
		System.out.println(Integer.toBinaryString(bin ^ 0b100));	// ou exclusif ->  10001
		
		// Opérateur de décalage
		System.out.println(Integer.toBinaryString(bin<<2)); 	// décallage à gauche de 2 -> 1010100
		System.out.println(Integer.toBinaryString(bin>>2)); 	// décallage à gauche de 1, insère à droite du bit de signe -> 101
		System.out.println(Integer.toBinaryString(bin>>>2)); 	// décallage à gauche de 3, insère à droite un 0 ->  101
		
        // Promotion numérique
		int pr1=3;
		long pr2=23L;
		double pr3=4.5;
		
		// 1=> Le type le + petit est promu vers le + grand type des deux
		long respr=pr1+pr2;	// 4=> Après une promotion le résultat aura le même type
		System.out.println(respr);

		// 2=> La valeur entière est promue en virgule flottant
		double respr2=pr1+pr3;
		System.out.println(respr2);
		
		int pr4=11;
		System.out.println(pr4/2); // 5
		System.out.println(pr4/2.0); //5.5
		
		// 3=> byte, short, char sont promus en int
		byte b1=2;
		byte b2=3;
		int respr3=b1+b2;
		byte b3=(byte)(b1+b2);
		System.out.println(respr3 + " " + b3);
		
		// Saisie dans la console => Scanner
		Scanner sc=new Scanner(System.in);
		int s1=sc.nextInt();
		double s2=sc.nextDouble();
		String str=sc.next();
		System.out.println(s1 + " " + s2 +" " + str);
		
        // Exercice Somme
        // Saisir 2 chiffres et afficher le résultat dans la console
        // sous la forme 1 + 3 = 4
        System.out.println("Entrer 2 entiers");
		int sa=sc.nextInt();
		int sb=sc.nextInt();
		int somme=sa+sb;
		System.out.println(sa + " + " + sb + " = " + somme);
		
		// Exercice Moyenne
        // Saisir 2 chiffres et afficher la moyenne des 2 nombres dans la console
		int sa1=sc.nextInt();
		int sb1=sc.nextInt();
		double moyenne=(sa1 +sb1)/2.0;	// ou ((double)(sa1 +sb1))/2
		System.out.println(moyenne);
		sc.close();	// Lorsque l'on ferme le scanner le scanner, on peut plus faire de saisie au clavier (même si l'on recrée un nouvel objet scanner)
        			// => le flux d'entré System.in est fermé
	}

}
