import java.util.Scanner;

public class Tableaux {

	public static void main(String[] args) {
		// Déclaration d'un tableau à une dimension
		// double [] tab=null; // On déclare une référence vers le tableau
		// tab=new double[6]; // On crée l'objet tableau de double

		double[] t = new double[6]; // ou double tab[]=new double[6];
		// Valeur d'initialisation des éléments du tableau
		// entier -> 0
		// double ou float -> 0.0
		// char -> '\u0000'
		// boolean -> false
		// référence -> null;

		// Accèder à un élément du tableau
		System.out.println(t[1]); // // accès au 2ème élément
		t[0] = 1.23; // accès au 1er élément
		System.out.println(t[0]);

		// Indice en dehors du tableau => génére une exception
		// t[100]=1.0;

		// Nombre d'élément du tableau
		System.out.println(t.length);

		// Parcourir un tableau avec un for
		for (int i = 0; i < t.length; i++) {
			System.out.println("t[" + i + "]=" + t[i]);
		}

		// Parcourir un tableau avec un foreach
		for (double elm : t) {
			System.out.println(elm);
		}

		// Déclaration et initialisation du tableau
		char[] tChr = { 'a', 'z', 'e', 'r' };
		for (char elm : tChr) {
			System.out.println(elm);
		}

		// Saisir la taille d tableau
		Scanner scan = new Scanner(System.in);
		int size = scan.nextInt();
		int tab[] = new int[size];
		System.out.println(tab.length);

		// Tableau 1D
		// - Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers:
		// -7,4,8,0,-3
		// - Modifier le programme pour faire la saisie de la taille du tableau et
		// saisir les éléments du tableau
		// int ti[]= {-7,-4,-8,-9,-3}; // 1

		int si = scan.nextInt(); // 2
		int ti[] = new int[si];
		for (int i = 0; i < si; i++) {
			System.out.print("T[" + i + "]");
			ti[i] = scan.nextInt();
		}
		int max = ti[0]; // Integer.MIN_VALUE;
		double somme = 0.0;
		for (int e : ti) {
			if (e > max) {
				max = e;
			}
			somme += e;
		}
		System.out.println("Maximum =" + max);
		System.out.println("Moyenne=" + (somme / ti.length));

		// Tableau à 2 dimensions

		// Déclaration d'un tableau à 2 dimensions
		int[][] tab2d = new int[3][4];
		// ou int tab2d [][]=new char[3][4];
		// ou int [] tab2d []=new char[3][4];

		// Accèder à un élément
		System.out.println(tab2d[1][2]);

		// Nombre d'élément sur la première dimension => nombre de ligne
		System.out.println(tab2d.length); // 3

		// Nombre de colonne de la première ligne
		System.out.println(tab2d[0].length); // 4

		// Parcourir un tableau à 2 dimensions avec un for
		for (int i = 0; i < tab2d.length; i++) {
			for (int j = 0; j < tab2d[0].length; j++) {
				System.out.print(tab2d[i][j] + " ");
			}
			System.out.println();
		}

		// Déclaration et initialisation d'un tableau en 2D
		String[][] tabStr = { { "aze", "rty" }, { "dfg", "uio" }, { "ghj", "klf" } };

		for (String[] ligne : tabStr) {
			for (String elm : ligne) {
				System.out.print(elm + " ");
			}
			System.out.println("");
		}
		scan.close();

		// Tableau en escalier
		// Déclaration d'un tableau en escalier => chaque ligne a un nombre d'élement différent
		String tabEsc[][] = new String[3][];
		tabEsc[0] = new String[3];
		tabEsc[1] = new String[2];
		tabEsc[2] = new String[4];

		// Parcourir un Tableau en escalier avec un for
		for (int i = 0; i < tabEsc.length; i++) {
			for (int j = 0; j < tabEsc[i].length; j++) {
				System.out.print(tabEsc[i][j]);
			}
			System.out.println();
		}

		// Parcourir un Tableau en escalier avec un foreach
		for (String[] ligne : tabEsc) {
			for (String elm : ligne) {
				System.out.print(elm + " ");
			}
			System.out.println("");
		}
	}

}
