package fr.dawan.formation;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class RendeurMonnaie {
	
	private static final int[] valeurPieces = { 200, 100, 50, 20, 10, 5, 2, 1 };
	
	private int[] stockPieces= {10,10,10,10,10,10,10,10};

	public Map<Double, Integer> rendre(double sommeEuro) throws Exception
    {
		Map<Double,Integer> pieces=new HashMap<>();
        int val = (int)(sommeEuro * 100.0);
        int i = 0;
        while (val > 0.0 && i < valeurPieces.length)
        {
            int nbPiece = val / valeurPieces[i];
            if (nbPiece != 0)
            {
                if (nbPiece > stockPieces[i])
                {
                    nbPiece = stockPieces[i];
                }
                pieces.put(valeurPieces[i] / 100.0, nbPiece);
                val -= nbPiece * valeurPieces[i];
                stockPieces[i] -= nbPiece;
            }
            i++;
        }
        if (val > 0.0)
        {
            throw new Exception("Il n'y a pas assez de pièce dans le stock  reste:"+ (val / 100.0) + "euros");
        }
        return Collections.unmodifiableMap(pieces);
    }
	
	public double sommeEnStock() {
		double somme=0.0;
		for(int i=0;i<valeurPieces.length;i++) {
			somme+=stockPieces[i]*valeurPieces[i];
		}
		return somme/100.0;
	}
	
	public boolean peutRendre(double somme) {
		return somme<=sommeEnStock();
	}

	public void recharger() {
		Arrays.fill(stockPieces, 10);
	}
	
	// Stocke Ilimité
	public static Map<Double, Integer> rendreStockInifinie(double sommeEuro){
		Map<Double,Integer> pieces=new HashMap<>();
		 int val = (int)(sommeEuro * 100.0);
         int i = 0;
         while (val > 0.0 && i < valeurPieces.length)
         {
             int tmp = val / valeurPieces[i];
             if (tmp != 0)
             {
                 pieces.put(valeurPieces[i] / 100.0, tmp);
                 val -= tmp * valeurPieces[i];
             }
             i++;
         }
         // Collections.unmodifiableMap => la map qui retourné n'est accessible qu'en lecture
         return Collections.unmodifiableMap(pieces);
	}
	
	public static void Afficher(Map<Double, Integer> pieces) {
		Set<Entry<Double,Integer>> entries=pieces.entrySet();
		for(Entry<Double,Integer> en : entries) {
			System.out.print(en.getKey() + "€\t");
		}
		System.out.println();
		for(Entry<Double,Integer> en : entries) {
			System.out.print(" "+en.getValue()+" \t");
		}
		System.out.println();
	}
}
