package fr.dawan.formation;

import java.util.StringTokenizer;

public class Application {

	public static void main(String[] args) {
		
		// Acronyme
		System.out.println(acronyme("Comité international olympique"));
		System.out.println(acronyme("Organisation du traité de l'Atlantique Nord"));
		System.out.println(acronyme("développement d'application web en aglomération nantaise"));
		
		// Cercle
        Point p = new Point(2, 0);
        Point p1 = new Point(0, 1);
        Point p2 = new Point(1, 1);

        Cercle c1 = new Cercle(p, 2.0);
        Cercle c2 = new Cercle(new Point(8, 0), 2.0);
        Cercle c3 = new Cercle(new Point(3, 0), 2.0);

        System.out.println(c1.collision(c2));
        System.out.println(c1.collision(c3));

        System.out.println(c1.contenu(p2));
        p1.setX(10);
        System.out.println(c1.contenu(p1));

		
		// Rendeur de monnaie
		
		// 1. Stock infinie
		RendeurMonnaie.Afficher(RendeurMonnaie.rendreStockInifinie(23.85));
		
		// 2. avec un stock de pièce 
		RendeurMonnaie rm=new RendeurMonnaie();
		System.out.println("Somme en stock=" +rm.sommeEnStock());
		System.out.println((rm.peutRendre(35.3)? "Peut ":"Ne peut pas" )+ " rendre 35,3€");
		System.out.println((rm.peutRendre(40.0)? "Peut ":"Ne peut pas" )+ " rendre 40€");
		try {
			RendeurMonnaie.Afficher(rm.rendre(35.3));
		} catch (Exception e1) {
			System.err.println(e1.getMessage());
		}
		System.out.println("Somme en stock=" + rm.sommeEnStock());
		rm.recharger();
		System.out.println("Somme en stock=" + rm.sommeEnStock());
	
		// Tic tac toe
		Grille g=new Grille();
		g.afficher();
	}
	
	// Faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme       
    // Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres

    // Ex: Comité international olympique → CIO
    //    Organisation du traité de l'Atlantique Nord → OTAN
    public static String acronyme(String str)
    {
        StringBuilder sb = new StringBuilder();
        StringTokenizer tok=new StringTokenizer(str.toUpperCase().trim()," '");
        while(tok.hasMoreElements()) {
        	String elm=tok.nextToken();
        	if(elm.length()>2) {
        		sb.append(elm.charAt(0));
        	}
        }
        return sb.toString();
    }

}
