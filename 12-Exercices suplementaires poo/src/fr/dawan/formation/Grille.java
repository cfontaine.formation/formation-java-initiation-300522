package fr.dawan.formation;

import java.util.Arrays;

public class Grille {
	private char[][] elements;

	public Grille() {
		this(3);
	}

	public Grille(int size) {
		this(size, size);
	}

	public Grille(int largeur, int hauteur) {
		elements = new char[largeur][hauteur];
		for (char[] e : elements) {
			Arrays.fill(e, ' ');
		}
	}

	public int getLargeur() {
		return elements[0].length;
	}

	public int getHauteur() {
		return elements.length;
	}

	public boolean isSquare() {
		return elements[0].length == elements.length;
	}

	public char getElement(int ligne, int colonne) {
		return elements[ligne][colonne];
	}

	public void setElement(int ligne, int colonne, char elm) {
		elements[ligne][colonne] = elm;
	}

	public boolean isEmpty(int ligne, int colonne) {
		return elements[ligne][colonne] == ' ';
	}

	public void afficher() {
		for (int l = 0; l < elements.length; l++) {
			for (int c = 0; c < elements[l].length; c++) {
				System.out.print("[" + elements[l][c] + "]");
			}
			System.out.println();
		}
	}

	public boolean testLigne(int ligne) {
		if (elements[ligne][0] == ' ') {
			return false;
		}
		char c = elements[ligne][0];
		for (int i = 1; i < elements[ligne].length; i++) {
			if (elements[ligne][i] != c) {
				return false;
			}
		}
		return true;
	}

	public boolean testColonne(int colonne) {
		if (elements[0][colonne] == ' ') {
			return false;
		}
		char c = elements[0][colonne];
		for (int i = 1; i < elements.length; i++) {
			if (elements[i][colonne] != c) {
				return false;
			}
		}
		return true;
	}

	public boolean testDiagonales() {
		// Uniquement si la grille est un carré
		if (!isSquare()) {
			return false;
		}
		char cDiagA = elements[0][0];
		char cDiagB = elements[0][elements.length - 1];
		boolean testA = cDiagA != ' ';
		boolean testB = cDiagB != ' ';
		for (int i = 1; i < elements.length; i++) {
			testA &= elements[i][i] == cDiagA;
			testB &= elements[i][elements.length - i - 1] == cDiagB;
		}
		return testA || testB;
	}
}
