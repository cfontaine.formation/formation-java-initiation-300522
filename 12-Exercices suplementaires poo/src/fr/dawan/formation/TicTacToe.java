package fr.dawan.formation;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TicTacToe {

	private Grille grille = new Grille();

	private char joueur = 'O';

	private Scanner sc;

	public TicTacToe(Scanner sc) {
		this.sc = sc;
	}

	public void jouer() {
		int coup = 0;
		do {
			grille.afficher();
			changerJoueur();
			System.out.println("Joueur " + joueur + " :");
			saisirCase();
			coup++;
		} while (coup < 9 && !testVictoire());
		grille.afficher();
		if (coup == 9) {
			System.out.println("Les joueurs sont a égalité");
		} else {
			System.out.println("Le joueur " + joueur + " a gagné");
		}
	}

	private void saisirCase() {
		int l, c;
		while (true) {
			l = saisieValide();
			c = saisieValide();
			if (grille.isEmpty(l, c)) {
				break;
			}
			System.out.println("La case " + (l + 1) + "," + (c + 1) + " a déjà été joué");
		}
		grille.setElement(l, c, joueur);
	}

	private boolean testVictoire() {
		return testHorizontal() || testVertical() || grille.testDiagonales();
	}

	private void changerJoueur() {
		joueur = joueur == 'X' ? 'O' : 'X';
	}

	private boolean testHorizontal() {
		for (int l = 0; l < grille.getHauteur(); l++) {
			if (grille.testLigne(l)) {
				return true;
			}
		}
		return false;
	}

	private boolean testVertical() {
		for (int c = 0; c < grille.getLargeur(); c++) {
			if (grille.testColonne(c)) {
				return true;
			}
		}
		return false;
	}

	private int saisieValide() {
		while (true) {
			try {
				int v = sc.nextInt();
				if (v > 0 && v <= 3) {
					return v - 1;
				} else {
					System.out.println("Il faut saisir un nombre entre 1 et 3");
				}
			} catch (InputMismatchException e) {
				System.err.println(e.getMessage());
				sc.nextLine(); // vider le buffer
			}
		}
	}

	public static void main(String arg[]) {
		Scanner scan = new Scanner(System.in);
		TicTacToe game = new TicTacToe(scan);
		game.jouer();
		scan.close();
	}
}
