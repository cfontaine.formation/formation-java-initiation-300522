package fr.dawan.formation;

import java.util.Date;

import fr.dawan.formation.utilitaires.Controller;
import static fr.dawan.formation.utilitaires.TestImportStatic.setMessage;


public class Application {

	public static void main(String[] args) {
		// Exercice: Point
		Point a=new Point();
		Point b=new Point(3,1);
		a.afficher();
		a.translation(0, 1);
		a.afficher();
		System.out.println(Point.distance(a, b));
		
		// package / import
		Controller ctx=new Controller();
		
		Date d=new Date();
		java.sql.Date dSql=new java.sql.Date(123L);
		
		//TestImportStatic.setMessage("hello"); // avec import
		setMessage("hello"); // avec import static 
		
	}

}
