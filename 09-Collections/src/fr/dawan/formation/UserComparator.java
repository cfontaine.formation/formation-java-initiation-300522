package fr.dawan.formation;

import java.util.Comparator;

public class UserComparator implements Comparator<User> {

	@Override
	public int compare(User u1, User u2) {
		// Comparaison pour avoir un classement dans l'ordre alphabétique inversé
		int cmp = u1.getNom().compareTo(u2.getNom());
		if (cmp == 0) {
			cmp = u2.getPrenom().compareTo(u2.getPrenom());
		}
		return cmp;
	}

}
