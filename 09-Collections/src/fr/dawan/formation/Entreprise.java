package fr.dawan.formation;

import java.util.ArrayList;
import java.util.List;

public class Entreprise {

	private List<Employe> employes = new ArrayList<>();

	private String nom;

	public Entreprise(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Employe> getEmployes() {
		return employes;
	}

	public void ajouter(Employe e) {
		employes.add(e);
	}

	public void retirer(Employe e) {
		employes.remove(e);
	}

	public int getNbEmploye() {
		return employes.size();
	}

	public double totalSalaireMensuel() {
		double somme = 0.0;
		for (int i = 0; i < employes.size(); i++) {
			somme += employes.get(i).getSalaire();
		}
		return somme;
	}

	public double totalSalaireMensuel(TypeContrat tc) {
		double somme = 0.0;
		for (int i = 0; i < employes.size(); i++) {
			Employe e = employes.get(i);
			if (tc == e.getContrat()) {
				somme += e.getSalaire();
			}
		}
		return somme;
	}

	public double salaireMoyen() {
		return totalSalaireMensuel() / employes.size();
	}

	@Override
	public String toString() {
		return "Entreprise [nom=" + nom + "nombre d'employé=" + employes.toString() + " ]";
	}

}
