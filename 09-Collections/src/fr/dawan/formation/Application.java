package fr.dawan.formation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Application {

	public static void main(String[] args) {
		// Avant java SE 5 => on pouvait stocker tous les objets qui héritent de Object
		// dans une collection
		List lst1 = new ArrayList(); // List => interface , ArrayList =>objet "réel" qui implémente l'interface
		lst1.add("Hello");
		lst1.add(12); // autoboxing convertion automatique type primtif -> objet de type enveloppe
						// implicitement new Integer(12)
		lst1.add(3.5);

		// get => récupérer un objet stocké dans la liste à l'indice 0
		// retourne des objects, il faut tester si l'objet correspont à la classe et le
		// caster

		if (lst1.get(0) instanceof String) {
			String str = (String) lst1.get(0);
			System.out.println(str);
		}

		// parcourir avec un iterateur
		Iterator iter = lst1.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

		// Type Générique
		// classe générique
		Box<String> b1 = new Box<>("Hello");
		String str2 = b1.getV();
		Box<Integer> b2 = new Box<>(12);

		// Méthode Générique
		// Pas besoin de préciser le type, il est déduit à partir du type des paramètres
		System.out.println(egal(12, 34));
		System.out.println(egal("az", "az"));

		// Java 5 => les collection utilise les types générique
		List<String> lstStr = new ArrayList<>(); // la liste ne peut plus contenir que des chaines de caractères
		lstStr.add("Hello");
		// lstStr.add(12); // => Erreur
		lstStr.add("aaaa");
		lstStr.add("Bonjour");
		lstStr.add(0, "Premier"); // insertion à l'index 0 uniquement pour les listes
		lstStr.add("Asupprimer");
		lstStr.add("AsupprimerIndex");
		lstStr.add("World");

		// get => revoie l'élément placé à l'index 2
		System.out.println(lstStr.get(2)); // Bonjour

		// size => le nombre d'élément de la collection
		System.out.println(lstStr.size()); // 7

		// set => remplace l'élément à l'index 1 par celui passé en paramètre
		lstStr.set(1, "Monde");

		// supprime le premier élément "asupprimer"
		lstStr.remove("Asupprimer");
		// Suppprimer l'élement à l'index 4
		lstStr.remove(4);

		// Parcourir une collection "foreach"
		for (String s : lstStr) {
			System.out.println(s);
		}

		// Set => pas de doublons
		Set<Double> st1 = new HashSet<>();
		st1.add(1.2);
		st1.add(3.4);
		System.out.println(st1.add(5.6)); // true
		// add retourne false => 1.2 existe déjà dans la collection
		System.out.println(st1.add(1.2)); // false

		for (double d : st1) {
			System.out.println(d);
		}

		// SortedSet => tous les objets sont automatiquement triés lorsqu'ils sont
		// ajoutés

		// * Interface Comparable
		// La classe (User) doit implémenter l'interface Comparable pour pouvoir faire
		// les comparaisons entre objet
		SortedSet<User> stUser1 = new TreeSet<>();
		stUser1.add(new User("John", "Doe"));
		stUser1.add(new User("Alan", "Smithee"));
		stUser1.add(new User("Jo", "Dalton"));
		stUser1.add(new User("Jane", "Doe"));

		for (User u : stUser1) {
			System.out.println(u);
		}
		System.out.println("----------");

		// * Interface Comparator
		// Un Comparator est externe à la classe (user) dont on veut comparer les //
		// éléments
		// On peut créer plusieurs classes implémentant Comparator pour comparer de
		// manière différente les objets ( différent: attributs, sens)

		// L'objet Comparator est passé au constructeur de TreeSet
		// Pour le définir:

		// - On peut utiliser une classe anonyme

//		SortedSet<User> stUser2=new TreeSet<>(new Comparator<User>() {
//
//			@Override
//			public int compare(User u1, User u2) {
//				int cmp=u1.getNom().compareTo(u2.getNom());
//				if(cmp==0) {
//					cmp=u2.getPrenom().compareTo(u2.getPrenom());
//				}
//				return -cmp;
//			}});

		// - Ou on peut utiliser une classe qui impléménte L'interface Comparator
		SortedSet<User> stUser2 = new TreeSet<>(new UserComparator());
		stUser2.add(new User("John", "Doe"));
		stUser2.add(new User("Alan", "Smithee"));
		stUser2.add(new User("Jo", "Dalton"));
		stUser2.add(new User("Jane", "Dupont"));

		for (User u : stUser2) {
			System.out.println(u);
		}

		// Queue => file d'attente
		Queue<Integer> pileFifo = new LinkedList<>();
		pileFifo.offer(2);
		pileFifo.offer(123);
		pileFifo.offer(4);

		// peek => obtenir l'élément disponible, sans le retirer de la file d'attente
		System.out.println(pileFifo.peek());
		System.out.println(pileFifo.peek());
		// pool => obtenir l'élément disponible et le retirer de la file d'attente
		System.out.println(pileFifo.poll());
		System.out.println(pileFifo.poll());
		System.out.println(pileFifo.poll());
		System.out.println(pileFifo.poll());

		// Map => association Clé/Valeur
		Map<String, User> m = new HashMap<>();
		m.put("st-123", new User("John", "Doe")); // put => ajouter la clé "st-123" et la valeur associé (objet User)
		m.put("st-585", new User("Jane", "Doe"));
		m.put("st-345", new User("Jo", "Dalton"));
		m.put("st-789", new User("Jack", "Dalton"));

		// get -> Obtenir la valeur associé à la clé st-585
		System.out.println(m.get("st-585"));

		// containsKey => la comparaisson se fait avec equals
		// -> false, la clé fr-62 n'est pas présente dans la map
		System.out.println(m.containsKey("st-62"));
		// -> true,la valeur est présente dans la map
		System.out.println(m.containsValue(new User("Jo", "Dalton")));

		// si la valeur existe déjà, elle est écrasée par la nouvelle valeur associé à
		// la clé
		m.put("st-789", new User("Alan", "Smithee"));
		System.out.println(m.get("st-789"));

		// keySet => retourne toutes les clés de la map
		Set<String> keys = m.keySet();
		for (String k : keys) {
			System.out.println(k);
		}

		// values => retourne toutes les valeurs de la map
		Collection<User> values = m.values();
		for (User u : values) {
			System.out.println(u);
		}

		// Entry => objet qui contient une clé et la valeur associée
		Set<Entry<String, User>> ent = m.entrySet();
		for (Entry<String, User> e : ent) {
			System.out.println(e.getKey() + " " + e.getValue());
		}

		// Classe Collections => Classe utilitaires pour les collections
		System.out.println(Collections.min(lstStr));

		Collections.sort(lstStr);
		for (String s : lstStr) {
			System.out.println(s);
		}

		// Classe Arrays => Classe utilitaires pour les tableaux
		String[] tabStr = new String[5];
		Arrays.fill(tabStr, "Bonjour"); // => initialiser un tableau avec une valeur

		System.out.println(Arrays.toString(tabStr)); // toString => afficher un tableau
		int tab1[] = { 1, 5, 7, 3, 8 };
		int tab2[] = { 1, 5, 7, 3, 8 };
		System.out.println(Arrays.equals(tab1, tab2)); // equals => comparaison de deux tableaux
		Arrays.sort(tab2); // sort => tri d'un tableau
		System.out.println(Arrays.toString(tab2));

		System.out.println(Arrays.binarySearch(tab2, 5)); // binarySearch => recherche d'un élément dans un tableau trié
		int index = Arrays.binarySearch(tab2, 6);
		System.out.println((index + 1) * -1);

		// Exercice Entreprise
		Entreprise en = new Entreprise("Acme");
		en.ajouter(new Employe("John", "Doe", TypeContrat.CDI, LocalDate.of(1997, 12, 9), 2500.0));
		en.ajouter(new Employe("Jane", "Doe", TypeContrat.CDI, LocalDate.of(1999, 1, 19), 2900.0));
		en.ajouter(new Employe("Alan", "Smithee", TypeContrat.CDD, LocalDate.of(1981, 6, 12), 3000.0));
		en.ajouter(new Employe("Jo", "Dalton", TypeContrat.STAGE, LocalDate.of(1989, 3, 14), 1800.0));
		System.out.println("nombre d'employé=" + en.getNbEmploye());
		System.out.println("total salaire=" + en.totalSalaireMensuel());
		System.out.println("total salaire CDI=" + en.totalSalaireMensuel(TypeContrat.CDI));
		System.out.println("Salaire moyen=" + en.salaireMoyen());
	}

	// Méthode générique

	static <T> boolean egal(T a, T b) {
		return a.equals(b);
	}
}
