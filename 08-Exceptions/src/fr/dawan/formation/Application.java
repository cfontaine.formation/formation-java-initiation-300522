package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		FileInputStream fis = null;
		// Checked exception => on est obligé (par java) de traiter l'exception
		try {
			// ...
			// ouverture d'un fichier qui n'existe pas => lance une exception
			// FileNotFoundException
			fis = new FileInputStream("nexistepas");
			int a = fis.read(); // read peut lancer une IOException
			System.out.println("Suite de code");
			// pour un bloc try, il peut y avoir plusieurs blocs catch pour traiter
			// diférentes exceptions de la - à la + générale
		} catch (FileNotFoundException e) { // si une exception FileNotFoundException ce produit dans le bloc try
			System.err.println("Le fichier n'existe pas");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) { // attrape tous les exceptions autre que FileNotFoundException et IOException
			System.err.println("Une autre exception");
		} finally { // le bloc finally est toujours exécuté
			System.out.println("Liberer les ressources");
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		// Runtime Exception => on peut traiter l'exception ou pas (pas d'obligation)
		try {
			int tab[] = new int[5];
			tab[40] = 12; // 10 =>ArrayIndexOutOfBoundsException
			System.out.println("Suite de code");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("Indice en dehors du tableau");
		}

		Scanner sc = new Scanner(System.in);
		try {
			int age = sc.nextInt();
			traitementEmployee(age);
		} catch (InputMismatchException e) {
			System.err.println("erreur de saisie");
		} catch (EmployeException e) {
			System.err.println("traitement age negatif main");
			e.printStackTrace();
		}
			
		// Exercice calculatrice
		double v1=sc.nextDouble();
		String op=sc.next();
		double v2=sc.nextDouble();
		try {
			System.out.println(calculatrice(v1, op, v2));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	
		System.out.println("fin de programme");
		sc.close();
	}

	public static void traitementEmployee(int age) throws EmployeException {
		System.out.println("Début traitement employé");
		try {
			traitementAge(age);
		} catch (AgeNegatifException e) {
			// traitement localement
			System.err.println("traitement local partiel: age négatif");
			// throw e; // Relancer une exception
			throw new EmployeException("Erreur traitement employé", e); // Relancer une exception d'un autre type
		}
		System.out.println("Fin traitement employé");
	}

	public static void traitementAge(int age) throws AgeNegatifException {
		System.out.println("Début traitement age");
		if (age < 0) {
			throw new AgeNegatifException(age); // throw new Exception("age négatif");
		}
		System.out.println("Fin traitement age");
	}

	// Exercice calculatrice
	public static double calculatrice(double v1, String op, double v2) throws Exception {
		switch (op) {
		case "+":
			return v1 + v2;
		case "-":
			return v1 - v2;
		case "*":
			return v1 * v2;
		case "/":
			if (v2 == 0.0) {
				throw new ArithmeticException("Division par zéro");
			}
			return v1 / v2;
		default:
			throw new Exception(op + " est un opérateur incorrect");
		}
	}

}
