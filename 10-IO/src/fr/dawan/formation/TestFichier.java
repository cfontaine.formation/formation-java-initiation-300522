package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.enums.TypeContrat;

public class TestFichier {

	public static void main(String[] args) {
		// Parcourir le répertoire du projet 02-Poo
		// File : représentation des chemins d'accès aux fichiers et aux répertoires
		parcourir(new File("..\\02-Poo"));

		// File.separator, File.separatorChar -> Le caractère de séparation par défaut
		// dépendant du système
		System.out.println(File.separatorChar); // caractère
		System.out.println(File.separator); // Chaine de caractère

		// Fichier texte
		// Ecriture
		ecrireFichierText("Test1.txt");
		ecrireFichierBufferText("Test2.txt");
		ecrireFichierTextPrint("Test3.txt");

		// Lecture
		lireFichierText("test.txt");

		List<String> line = lireFichierTextBuff("test2.txt");
		for (String s : line) {
			System.out.println(s);
		}

		// Fichier Binaire
		// ecriture
		byte[] tb= {1,3,10,5,3,6,1,34,5,8,13,54,34};
		ecrireFichierBin("test.bin", tb);
		List<Byte> lstData=lireFichierBin("test.bin");
		for(byte b :lstData) {
			System.out.println(b);
		}
		// Exercice Copie de fichier
		copier("logo.png", "logo_copie.png");

		// Exercice Export/Import Csv
		List<Employe> employes = new ArrayList<Employe>();
		employes.add(new Employe("John", "Doe", TypeContrat.CDI, LocalDate.of(1997, 12, 9), 2500.0));
		employes.add(new Employe("Jane", "Doe", TypeContrat.CDI, LocalDate.of(1999, 1, 19), 2900.0));
		employes.add(new Employe("Alan", "Smithee", TypeContrat.CDD, LocalDate.of(1981, 6, 12), 3000.0));
		employes.add(new Employe("Jo", "Dalton", TypeContrat.STAGE, LocalDate.of(1989, 3, 14), 1800.0));

		try {
			ExportImportEmploye.exportCsv("employe.csv", employes);
			List<Employe> ets = ExportImportEmploye.importCsv("employe.csv");
			for (Employe em : ets) {
				System.out.println(em);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// Parcourir un système de fichiers
	public static void parcourir(File f) {
		if (f.exists()) { // Test l'existence du fichier ou du dossier
			if (f.isDirectory()) { // si c'est un dossier
				System.out.println("");
				System.out.println("> Répertoire= " + f.getName()); // affichage du nom du répertoire

				File[] tf = f.listFiles(); // Récupération du contenu du dossier
				for (File fi : tf) {
					parcourir(fi); // Appel récursif sur chaque fichier du dossier
				}
			} else {
				System.out.println("  " + f.getName()); // affichage du nom du fichier
			}
		} else {
			f.mkdir(); // si le chemin n'existe pas on crée un dossier
		}
	}

	public static void ecrireFichierText(String path) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(path); // Ouverture du flux
			// par défaut, le fichier sera écrasé, si il existe déjà
			// ou
			// fw = new FileWriter(path, true);
			// true -> indique que le fichier sera complété
			for (int i = 0; i < 10; i++) {
				fw.write("Hello World"); // Ecriture d'une chaine dans le fichier
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close(); // Fermeture du flux
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// Utilisation de try with ressource pour fermer automatiquement le flux vers le
	// fichier (à partir du java 7)
	// try with ressource fonctionne avec tous les objets implémentant l'interface
	// AutoCloseable
	public static void ecrireFichierBufferText(String path) {
		// On utilise un BufferedWriter, il n'a pas accès directement au fichier, il
		// faut passer par un FileWriter
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true))) { // append => true : on ajoute des
																					// données au fichier
			for (int i = 0; i < 10; i++) { // false : lefichier est écrasé (par défaut)
				bw.write("Hello World");
				bw.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Ecrire dans un fichier texte avec PrintWriter
	public static void ecrireFichierTextPrint(String chemin) {
		try (PrintWriter pw = new PrintWriter(new FileWriter(chemin, true))) {
			for (int i = 0; i < 10; i++) {
				pw.println(i);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Lecture d'un fichier texte
	public static void lireFichierText(String chemin) {
		char[] buff = new char[10];
		try (FileReader fr = new FileReader(chemin)) {
			for (;;) {
				int numRead = fr.read(buff); // lecture de 10 caractères maximum dans le fichier
				if (numRead == -1) { // lorsque l'on atteint la fin du fichier read retourne -1
					break;
				}
				for (int i = 0; i < numRead; i++) {
					System.out.println(buff[i]);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Lecture d'un fichier texte avec BufferedReader
	public static List<String> lireFichierTextBuff(String path) {
		List<String> lst = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			for (;;) { // boucle infinie
				String row = br.readLine(); // lecture d'une ligne dans le fichier
				if (row == null) { // lorsque l'on atteint la fin du fichier readLine retourne null
					break;
				}
				lst.add(row);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lst;
	}

	// Ecrire un fichier binaire
	public static void ecrireFichierBin(String path,byte[] data) {
		try(FileOutputStream fos= new FileOutputStream(path)){
			fos.write(data);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	// Lire un fichier binaire
	public static List<Byte> lireFichierBin(String path) {
		List<Byte> data = new ArrayList<>();
		int t = 0;
		try (FileInputStream fis = new FileInputStream(path)) {
			while (true) {
				t = fis.read();
				if (t == -1) {
					break;
				}
				data.add((byte) t);
			}
		}  catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

//    Exercice Copie
//    écrire une méthode de classe qui permet de copie un fichier binaire octet par octet
//    void copier(String pathSource,String pathTarget)
	public static void copier(String pathSource, String pathTarget) {
		try (FileInputStream fis = new FileInputStream(pathSource);
				FileOutputStream fos = new FileOutputStream(pathTarget)) {
			while (true) {
				int b = fis.read();
				if (b == -1) {
					break;
				}
				fos.write(b);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
