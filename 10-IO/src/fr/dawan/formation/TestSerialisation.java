package fr.dawan.formation;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fr.dawan.formation.beans.Adresse;
import fr.dawan.formation.beans.Personne;

public class TestSerialisation {

	public static void main(String[] args) {
		Adresse adr = new Adresse("46,rue des cannoniers", "Lille", "59800");
		Personne per = new Personne("Jo", "Dalton", adr);
		per.setPassword("123456");
		serialisation(per, "personne.bin");
		// serialisationXML(per, "personne.xml");
		Personne perR = deSerialisation("personne.bin");
		// Personne perR = deSerialisationXML("personne.xml");
		if (perR != null) {
			System.out.println(perR);
			System.out.println("password=" + perR.getPassword());
		}
	}

	// Sérialisation
	// l'objet Personne va être sérialiser ainsi que tous les objets qu'il contient
	// ObjectOutputStream permet de persiter un objet ou une grappe d'objet
	public static void serialisation(Personne per, String chemin) {
		try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(chemin))) {
			os.writeObject(per);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Désérialisation
	// ObjectInputStream permet de désérialiser
	public static Personne deSerialisation(String chemin) {
		try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(chemin))) {
			Object obj = is.readObject();
			if (obj instanceof Personne) {
				return (Personne) obj;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Sérialisation XML
	// Pour pouvoir sérializer un objet en xml avec XMLEncoder, il doit avoir un
	// constructeur par défaut
	// transcient ne fonction ne pas avec la sérialisation xml
	public static void serialisationXML(Personne per, String chemin) {
		try (XMLEncoder os = new XMLEncoder(new FileOutputStream(chemin))) {
			os.writeObject(per);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Désérialisation XML => XMLDecoder
	public static Personne deSerialisationXML(String chemin) {
		try (XMLDecoder is = new XMLDecoder(new FileInputStream(chemin))) {
			Object obj = is.readObject();
			if (obj instanceof Personne) {
				return (Personne) obj;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
