package fr.dawan.formation;

public class Launch {

	public static void main(String[] args) {
		// Agrégation
		Voiture v1=new Voiture("Renault","blanc","az-4563-fg");
		v1.afficher();
		System.out.println("---------");
		Adresse adr1=new Adresse("46, rue des cannoniers","Lille","59000");
		Personne per1=new Personne("John","Doe");
		per1.setAdresse(adr1);
		per1.afficher();
		System.out.println("---------");
		v1.setProprietaire(per1);
		v1.afficher();
		System.out.println("---------");
		//
		Voiture v2=new Voiture("Ford","bleu","ww-366-fg",per1);
		v2.afficher();

		Personne per2=new Personne("Jane","Doe",adr1);
		per2.afficher();
		
		Personne per3=new Personne("Alan","Smithee",new Adresse("1 rue Esquermoise","Lille","59800"));
		per3.afficher();
		System.out.println("---------");
		
		// Héritage
		VoiturePrioritaire vp1=new VoiturePrioritaire();
		vp1.setMarque("Honda");
		vp1.accelerer(10);
		vp1.afficher();
		vp1.allumerGyro();
		System.out.println(vp1.isGyro());
		
		VoiturePrioritaire vp2=new VoiturePrioritaire("Kia", "gris", "Ze-1848-Ty", true);
		vp2.afficher();
		
		// Compte Expargne
		CompteEpargne ce1=new CompteEpargne(120, "John DOE", 2.4);
		ce1.afficher();
		ce1.calculInteret();
		ce1.afficher();
		
		// final
		System.out.println(Voiture.VITESSE_MAX);
		// Voiture.VITESSE_MAX=120;
		System.out.println(Math.PI);
	}

}
