package fr.dawan.formation;

public class CompteEpargne extends CompteBancaire {

	private double taux = 1.5;

	public CompteEpargne(double solde, String titulaire, double taux) {
		super(solde, titulaire);
		this.taux = taux;
	}

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	public void calculInteret() {
		solde *= (1 + taux / 100);
	}

	@Override
	public void afficher() {
		super.afficher();
		System.out.println("taux=" + taux);
	}

}
