package fr.dawan.formation;

public class VoiturePrioritaire extends Voiture {

	private boolean gyro;

	public VoiturePrioritaire() {
		// super(); // implicite
		System.out.println("Constructeur par Défaut: VoiturePrioritaire");
	}

	public VoiturePrioritaire(boolean gyro) {
		// super()
		this.gyro = gyro;
	}

	public VoiturePrioritaire(String marque, String couleur, String plaqueIma, boolean gyro) {
		super(marque, couleur, plaqueIma);
		System.out.println("Constructeur 3 paramètres : VoiturePrioritaire");
		this.gyro = gyro;
	}

	public VoiturePrioritaire(String marque, String couleur, String plaqueIma, int vitesse, int compteurKM) {
		super(marque, couleur, plaqueIma, vitesse, compteurKM);

	}

	public void allumerGyro() {
		gyro = true;
	}

	public void eteindreGyro() {
		gyro = false;
	}

	public boolean isGyro() {
		return gyro;
	}

	public void boost() {
		vitesse = vitesse * 2;
		// setVitesse(getVitesse()*2);
	}

	@Override
	public void afficher() {
		super.afficher();
		System.out.println("gyro=" + gyro);
	}

}
