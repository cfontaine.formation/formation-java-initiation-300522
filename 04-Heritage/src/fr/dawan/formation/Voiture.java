package fr.dawan.formation;
import java.io.Serializable;
// final  => interdire l'héritage
public /*final*/ class Voiture implements Serializable{
	private String marque;
	private String couleur = "Rouge";
	private String plaqueIma;
	protected int vitesse;
	private int compteurKM = 20;
	
	// agrégation
	private Personne proprietaire;

	private static int compteurVoiture;
	
	// final => constante
	public static final int VITESSE_MAX=255;

	// Constructeurs
	public Voiture() {
		compteurVoiture++;
		System.out.println("Constructeur par défaut : Voiture");
	}

	Voiture(String marque, String couleur, String plaqueIma) {
		this();
		System.out.println("Constructeur 3 paramètres : Voiture");
		this.marque = marque;
		this.couleur = couleur;
		this.plaqueIma = plaqueIma;
	}

	public Voiture(String marque, String couleur, String plaqueIma, int vitesse, int compteurKM) {
		this(marque, couleur, plaqueIma);
		System.out.println("Constructeur 5 paramètres");
		this.vitesse = vitesse;
		this.compteurKM = compteurKM;
	}

	public Voiture(String marque, String couleur, String plaqueIma, Personne proprietaire) {
		this(marque,couleur,plaqueIma);
		this.proprietaire = proprietaire;
	}

	// Getter et Setter
	public String getMarque() {
		return marque;
	}
	
	public int getVitesse() {
		return vitesse;
	}
	
	public void setVitesse(int vitesse) {
		if(vitesse>0) {
			this.vitesse=vitesse;
		}
	}
	
	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getPlaqueIma() {
		return plaqueIma;
	}

	public void setPlaqueIma(String plaqueIma) {
		this.plaqueIma = plaqueIma;
	}

	public int getCompteurKM() {
		return compteurKM;
	}

	public static int getCompteurVoiture() {
		return compteurVoiture;
	}

	// 
	public void setMarque(String marque) {
		this.marque = marque;
	}

	public void setCompteurKM(int compteurKM) {
		this.compteurKM = compteurKM;
	}

	public static void setCompteurVoiture(int compteurVoiture) {
		Voiture.compteurVoiture = compteurVoiture;
	}

	public Personne getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(Personne proprietaire) {
		this.proprietaire = proprietaire;
	}

	// Méthodes d'instance
	public void accelerer(int vAcc) {
		if (vAcc > 0) {
			vitesse += vAcc; 
		}
	}

	public void freiner(int vFrn) {
		if (vFrn > 0) {
			vitesse -= vFrn;
		}
	}

	public void arreter() {
		vitesse = 0;
	}

	public boolean estArreter() {
		return vitesse == 0;
	}

	// final => interdit la redéfinition de la méthode
	public /*final*/ void afficher() {
		System.out.println("Voiture[" + marque + " " + couleur + " " + plaqueIma + " " + vitesse + " " + compteurKM + "]");
		if(proprietaire!=null) {
			proprietaire.afficher();
		}
	}
	
	public static boolean egalVitesse(Voiture a,Voiture b) {
		return a.vitesse==b.vitesse;
	}
}
