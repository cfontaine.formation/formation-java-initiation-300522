package fr.dawan.formation;

public class Rectangle extends Forme {

	private double largeur;
	
	private double hauteur;
	
	public Rectangle(Couleur couleur,double largeur,double hauteur) {
		super(couleur);
		this.largeur=largeur;
		this.hauteur=hauteur;
	}

	public double getLargeur() {
		return largeur;
	}

	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}

	public double getHauteur() {
		return hauteur;
	}

	public void setHauteur(double hauteur) {
		this.hauteur = hauteur;
	}
	
	@Override
	public double calculSurface() {
		return largeur*hauteur;
	}

	@Override
	public String toString() {
		return "Rectangle [largeur=" + largeur + ", hauteur=" + hauteur + "]";
	}

}
