package fr.dawan.formation;

public class Application {

	public static void main(String[] args) {
		Terrain t = new Terrain();
		
		t.ajouter(new Rectangle(Couleur.BLEU, 1.0, 1.0));
		t.ajouter(new Rectangle(Couleur.BLEU, 1.0, 1.0));
		t.ajouter(new Cercle(Couleur.ROUGE, 1.0));
		t.ajouter(new Cercle(Couleur.ROUGE, 1.0));
		t.ajouter(new TriangleRectangle(Couleur.VERT, 1.0, 1.0));
		t.ajouter(new TriangleRectangle(Couleur.VERT, 1.0, 1.0));
		t.ajouter(new TriangleRectangle(Couleur.ORANGE, 1.0, 1.0));
		t.ajouter(new Rectangle(Couleur.ORANGE, 1.0, 1.0));
		
		System.out.println("Surface total=" + t.calculSurface());
		System.out.println("Surface Rouge=" + t.calculSurface(Couleur.ROUGE));
		System.out.println("Surface Vert=" + t.calculSurface(Couleur.VERT));
		System.out.println("Surface Bleu=" + t.calculSurface(Couleur.BLEU));
		System.out.println("Surface Orange=" + t.calculSurface(Couleur.ORANGE));
	}

}
