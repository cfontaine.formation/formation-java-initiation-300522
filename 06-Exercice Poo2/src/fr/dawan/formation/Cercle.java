package fr.dawan.formation;

public class Cercle extends Forme {

	private double rayon;

	public Cercle(Couleur couleur, double rayon) {
		super(couleur);
		this.rayon = rayon;
	}

	public double getRayon() {
		return rayon;
	}

	public void setRayon(double rayon) {
		this.rayon = rayon;
	}

	@Override
	public double calculSurface() {
		return Math.PI * rayon * rayon;
	}

	@Override
	public String toString() {
		return "Cercle [rayon=" + rayon + ", " + super.toString() + "]";
	}

}
