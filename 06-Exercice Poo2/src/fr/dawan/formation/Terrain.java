package fr.dawan.formation;

public class Terrain {

	private Forme[] formes = new Forme[10];

	private int nbForme;

	public void ajouter(Forme forme) {
		if (nbForme < formes.length) {
			formes[nbForme++] = forme;
			//	nbForme++;
		}

	}

	public double calculSurface() {
		double surface = 0.0;
		for (int i = 0; i < nbForme; i++) {
			surface += formes[i].calculSurface();
		}
		return surface;
	}

	public double calculSurface(Couleur c) {
		double surface = 0.0;
		for (int i = 0; i < nbForme; i++) {
			if (formes[i].getCouleur() == c) {
				surface += formes[i].calculSurface();
			}
		}
		return surface;
	}
}
