package fr.dawan.formation;

public class TriangleRectangle extends Rectangle {

	public TriangleRectangle(Couleur couleur, double largeur, double hauteur) {
		super(couleur, largeur, hauteur);
	}

	@Override
	public double calculSurface() {
		return super.calculSurface() / 2.0;
	}

	@Override
	public String toString() {
		return "TriangleRectangle [" + super.toString() + "]";
	}

}
