package fr.dawan.formation;

public class Canard extends Animal implements PeuxMarcher, PeuxVoler {

	public Canard(int poid, int age) {
		super(poid, age);
	}

	@Override
	public void decoller() {
		System.out.println("Le canard  décolle");

	}

	@Override
	public void atterir() {
		System.out.println("Le canard  atterit");

	}

	@Override
	public void marcher() {
		System.out.println("Le canard  marche");

	}

	@Override
	public void courir() {
		System.out.println("Le canard  courrir");

	}

	@Override
	public void emettreSon() {
		System.out.println("coin coin");

	}

}
