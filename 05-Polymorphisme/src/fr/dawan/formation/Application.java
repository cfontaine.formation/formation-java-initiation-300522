package fr.dawan.formation;

public class Application {

	public static void main(String[] args) {
		//Animal a1 = new Animal(3000, 4); // classe abstract => on ne peux plus l'instancié
		//a1.emettreSon();

		Chien ch1 = new Chien(10000, 3, "Rolo");
		ch1.emettreSon();

		Animal a2 = new Chien(5000, 7, "Laika");
		a2.emettreSon();
		System.out.println(a2.getAge());

		if (a2 instanceof Chien) {
			Chien ch2 = (Chien) a2;
			ch2.emettreSon();
			System.out.println(ch2.getNom());
		}
		
		Chat c1=new Chat(3500,5,5);
		
		Refuge refuge=new Refuge();
		refuge.ajouter(ch1);
		refuge.ajouter(a2);
		refuge.ajouter(c1);
		refuge.ecouter();
		
		// interface 
		PeuxMarcher[] tabInter=new PeuxMarcher[10];
		tabInter[0]=new Chien(7000,6,"Snoopy");
		tabInter[1]=new Chat(7000,6,3);
		tabInter[2]=new Canard(1600, 2);

		for(PeuxMarcher e : tabInter) {
			if(e!=null) {
				e.courir();
			}
		}
		
		PeuxVoler pv=new Canard(2000,1);
		pv.atterir();
		
		// Object
		// toString()
		Chien ch4=new Chien(4000,5,"Idefix");
		System.out.println(ch4.toString());
		System.out.println(ch4);
	
		// Equals et hashcode
		Chien ch5=new Chien(4000,5,"Idefix");
		System.out.println(ch4==ch5); // comparaison de référence -> false
		System.out.println(ch4.equals(ch5));
	
		// Clonable
		try {
			Chien ch6= (Chien)ch5.clone();
			ch5.setPoid(5000);
			System.out.println(ch5);
			System.out.println(ch6);
		} catch (CloneNotSupportedException e1) {
			e1.printStackTrace();
		}
		
		// Enumération
		Direction dir=Direction.NORD;
		System.out.println(dir.name()); // NORD
		System.out.println(dir.toString());
		
		Direction dir2=Direction.valueOf("SUD");
		System.out.println(dir2);
		
		System.out.println(dir.ordinal());
		System.out.println(dir2.ordinal());
		
		Direction[] tabDir=Direction.values();
		for(Direction d : tabDir) {
			System.out.println(d);
		}
	}

}
