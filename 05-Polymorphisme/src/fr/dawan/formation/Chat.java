package fr.dawan.formation;

public class Chat extends Animal implements PeuxMarcher {

	private int nbVie = 9;

	public Chat(int poid, int age, int nbVie) {
		super(poid, age);
		this.nbVie = nbVie;
	}

	public int getNbVie() {
		return nbVie;
	}

	public void setNbVie(int nbVie) {
		this.nbVie = nbVie;
	}

	@Override
	public void emettreSon() {
		System.out.println("Le chat miaule");
	}

	@Override
	public void marcher() {
		System.out.println("Le chat marche");

	}

	@Override
	public void courir() {
		System.out.println("Le chat court");
	}
}
