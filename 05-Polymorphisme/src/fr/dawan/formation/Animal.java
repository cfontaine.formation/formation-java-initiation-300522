package fr.dawan.formation;

import java.util.Objects;

// Classe abstraite 
public abstract class Animal { // implicitement Animal hérite de Object

	private int poid;
	
	private int age;

	public Animal(int poid, int age) {
		this.poid = poid;
		this.age = age;
	}

	public int getPoid() {
		return poid;
	}

	public void setPoid(int poid) {
		this.poid = poid;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
//	public void emettreSon() {
//		System.out.println("L'animal emet un son");
//	}
	
	// Méthode abstraite
	public abstract void emettreSon();

	@Override
	public String toString() {
		return "Animal [poid=" + poid + ", age=" + age + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(age, poid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		return age == other.age && poid == other.poid;
	}

	
	
	
//	@Override
//	protected void finalize() throws Throwable {
//		// TODO Auto-generated method stub
//		super.finalize();
//	}

	
	
	
}
